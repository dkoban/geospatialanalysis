Geospatial Analysis Workflows
================

# Convex Hulls to KML and Folium

## Given two groups of points

![alt text](images/convex_hull_points.png)

## Identify the convex hull that emcompasses them

![alt text](images/convex_hull_polys.png)

## Export them to kmz.

![alt text](images/google_earth_hull.png)

# Pocket Waffles

Split a bounding box into smaller bounding boxes

## Given a center point and buffer, generate a bounding box

![alt
text](images/bounding_box.png)

## Given a bounding box, cast a grid of points over the area at a fixed interval

![alt text](images/grid_box.png)

## Given a bounding box, divide the area into smaller bounding boxes

![alt text](images/pocket_waffle.png)

# Maritime Interpolation

## Observed path of a vessel

![alt text](images/observed_path.png)

## Inferred path of the vessel

![alt text](images/interpolated_path.png)
