# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 15:10:01 2020

@author: Tom
"""
import pandas as pd
import geopandas as gpd
import pygeohash as gh
import folium
from shapely import geometry
import math
import random
from datetime import datetime

def abi(p_choice , d_choice):    
    #Collect
    df = pd.read_csv("ACLED2.csv")
    df = df[['data_id', 'event_date', 'event_type', 'sub_event_type', 'notes', 'latitude', 'longitude', 'geo_precision', 'source']]
    df['latitude'] = df['latitude'] + [random.uniform(0, 0.002) for x in range(len(df))]
    df['longitude'] = df['longitude'] + [random.uniform(0, 0.002) for x in range(len(df))]
    armed_clashes = df[df['sub_event_type'] == "Armed clash"]
    suicide_attacks = df[df['sub_event_type'] == "Suicide bomb"]
    idf_attacks = df[df['sub_event_type'] == "Shelling/artillery/missile attack"]
    ied_attacks = df[df['sub_event_type'] == "Remote explosive/landmine/IED"]
    
    
    #Condition
    pd.options.mode.chained_assignment = None
    armed_clashes['date'] = armed_clashes['event_date'].apply(lambda x: datetime.strptime(x, '%d %B %Y'))
    suicide_attacks['date'] = suicide_attacks['event_date'].apply(lambda x: datetime.strptime(x, '%d %B %Y'))
    idf_attacks['date'] = idf_attacks['event_date'].apply(lambda x: datetime.strptime(x, '%d %B %Y'))
    ied_attacks['date'] = ied_attacks['event_date'].apply(lambda x: datetime.strptime(x, '%d %B %Y'))
    
    
    #filter
    province_shapes = gpd.read_file("provicinial-boundaries")
    provinces = province_shapes['PRV_NAME'].tolist()
    def geocode(df):
        gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.longitude, df.latitude))
        df = []
        for value in range(0,len(province_shapes)):    
            temp = gdf[gdf.geometry.intersects(province_shapes.iloc[value]['geometry'])]
            temp = temp.assign(province=province_shapes.iloc[value]['PRV_NAME'])
            df.append(temp)
        df = pd.concat(df)
        return(df)
    
    armed_clashes = geocode(armed_clashes)
    suicide_attacks = geocode(suicide_attacks)
    idf_attacks = geocode(idf_attacks)
    ied_attacks = geocode(ied_attacks)
    armed_clashes = armed_clashes[(armed_clashes['province'].eq(p_choice)) & (armed_clashes['geo_precision'] >= 2) & (armed_clashes['date'] == d_choice)]
    suicide_attacks = suicide_attacks[(suicide_attacks['province'].eq(p_choice)) & (suicide_attacks['geo_precision'] >= 2) & (suicide_attacks['date'] == d_choice)]
    idf_attacks = idf_attacks[(idf_attacks['province'].eq(p_choice)) & (idf_attacks['geo_precision'] >= 2) & (idf_attacks['date'] == d_choice)]
    ied_attacks = ied_attacks[(ied_attacks['province'].eq(p_choice)) & (ied_attacks['geo_precision'] >= 2) & (ied_attacks['date'] == d_choice)]
    
    
    #enrich
    fatality_events = pd.read_csv("ACLED2.csv")
    fatality_events = fatality_events[['data_id', 'fatalities']]
    fatality_events = fatality_events[fatality_events['fatalities'] >= 1]
    armed_clashes = armed_clashes.merge(fatality_events, how = "left", on = "data_id")
    armed_clashes = armed_clashes[armed_clashes['fatalities'] >= 1]
    suicide_attacks = suicide_attacks.merge(fatality_events, how = "left", on = "data_id")
    suicide_attacks = suicide_attacks[suicide_attacks['fatalities'] >= 1]
    idf_attacks = idf_attacks.merge(fatality_events, how = "left", on = "data_id")
    idf_attacks = idf_attacks[idf_attacks['fatalities'] >= 1]
    ied_attacks = ied_attacks.merge(fatality_events, how = "left", on = "data_id")
    ied_attacks = ied_attacks[ied_attacks['fatalities'] >= 1]
    armed_clashes['popup_text'] = "<b>Event: </b>" + armed_clashes['sub_event_type'] + "<br>" + \
                                  "<b>Date: </b>" + armed_clashes['date'].apply(lambda x: x.strftime("%m-%d-%Y")) + "<br>" + \
                                  "<b>Description: </b>" + armed_clashes['notes'] + "<br>" + \
                                  "<b>Fatalities: </b>" + armed_clashes['fatalities'].astype('str') + "<br>" + \
                                  "<b>Report Source: </b>" + armed_clashes['source'] 
    suicide_attacks['popup_text'] = "<b>Event: </b>" + suicide_attacks['sub_event_type'] + "<br>" + \
                                  "<b>Date: </b>" + suicide_attacks['date'].apply(lambda x: x.strftime("%m-%d-%Y")) + "<br>" + \
                                  "<b>Description: </b>" + suicide_attacks['notes'] + "<br>" + \
                                  "<b>Fatalities: </b>" + suicide_attacks['fatalities'].astype('str') + "<br>" + \
                                  "<b>Report Source: </b>" + suicide_attacks['source'] 
    idf_attacks['popup_text'] = "<b>Event: </b>" + idf_attacks['sub_event_type'] + "<br>" + \
                                  "<b>Date: </b>" + idf_attacks['date'].apply(lambda x: x.strftime("%m-%d-%Y")) + "<br>" + \
                                  "<b>Description: </b>" + idf_attacks['notes'] + "<br>" + \
                                  "<b>Fatalities: </b>" + idf_attacks['fatalities'].astype('str') + "<br>" + \
                                  "<b>Report Source: </b>" + idf_attacks['source'] 
    ied_attacks['popup_text'] = "<b>Event: </b>" + ied_attacks['sub_event_type'] + "<br>" + \
                                  "<b>Date: </b>" + ied_attacks['date'].apply(lambda x: x.strftime("%m-%d-%Y")) + "<br>" + \
                                  "<b>Description: </b>" + ied_attacks['notes'] + "<br>" + \
                                  "<b>Fatalities: </b>" + ied_attacks['fatalities'].astype('str') + "<br>" + \
                                  "<b>Report Source: </b>" + ied_attacks['source'] 
    
    
    #Merge
    armed_clashes['data_source'] = "armed_clash"
    suicide_attacks['data_source'] = "suicide_attacks"
    idf_attacks['data_source'] = "idf_attacks"
    ied_attacks['data_source'] = "ied_attacks"
    df = pd.concat([armed_clashes, suicide_attacks, idf_attacks, ied_attacks]).reset_index(drop = "index")
    
    
    #Summarize
    df['hash'] = df.apply(lambda x: gh.encode(x.latitude, x.longitude, precision = 6), axis = 1)
    density = df.groupby(['hash', 'data_source']).count().reset_index()[['hash', 'data_source']].groupby('hash').count().reset_index()
    def geohash_to_polygon(geo):
        lat_centroid, lng_centroid, lat_offset, lng_offset = gh.decode_exactly(geo)
        corner_1 = (lat_centroid - lat_offset, lng_centroid - lng_offset)[::-1]
        corner_2 = (lat_centroid - lat_offset, lng_centroid + lng_offset)[::-1]
        corner_3 = (lat_centroid + lat_offset, lng_centroid + lng_offset)[::-1]
        corner_4 = (lat_centroid + lat_offset, lng_centroid - lng_offset)[::-1]
        return geometry.Polygon([corner_1, corner_2, corner_3, corner_4, corner_1])
    density['geometry'] = density['hash'].apply(geohash_to_polygon)
    
    
    #Communicate
     # Create a leaflet map
    density['value'] = density['data_source']
    density['popup_txt'] =   "<b>Geohash: </b>" + density['hash'] + "<br>" + \
                             "<b>Data Source Count: </b>" + density['data_source'].astype('str')
    densityjson = gpd.GeoDataFrame(density, crs="EPSG:4326")
    densityjson = densityjson.to_json()
    
    # Add the World imagery tile
    url_base = 'https://server.arcgisonline.com/ArcGIS/rest/services/'
    service = 'World_Imagery/MapServer/tile/{z}/{y}/{x}'
    attribution = 'ESRI'
    tileset = url_base + service
    
    map_center = [34.2,70.8]
    my_map = folium.Map(location=(map_center[0], map_center[1]), tiles = None, zoom_start = 10)
    folium.TileLayer("cartodbdark_matter", name = "Dark Matter").add_to(my_map)
    folium.TileLayer(tileset, attr = attribution, name = "World Imagery").add_to(my_map)
    choropleth = folium.Choropleth(geo_data = densityjson,
                                   name = 'Density',
                                   data = density,
                                   columns = ['hash', 'data_source'],
                                   key_on = 'feature.properties.hash',
                                   fill_color = 'Reds',
                                   fill_opacity = 0.7,
                                   line_opacity = 0.2,
                                   legend_name = 'Density').add_to(my_map)
    choropleth.geojson.add_child(folium.features.GeoJsonTooltip(['popup_txt'],labels=False))
    fg1 = folium.FeatureGroup(name = 'Armed Clashes', show = False)
    for i in range(0,len(armed_clashes)):
        iframe = folium.IFrame(html = armed_clashes.iloc[i]['popup_text'], width = 400, height = 175)
        popup = folium.Popup(iframe, max_width = 400, parse_html = True)
        folium.CircleMarker([armed_clashes.iloc[i]['latitude'], armed_clashes.iloc[i]['longitude']], 
                      popup=popup, color = "blue", radius = 1).add_to(fg1)
    my_map.add_child(fg1)
    fg2 = folium.FeatureGroup(name = 'Suicide Attacks', show = False)
    for i in range(0,len(suicide_attacks)):
        iframe = folium.IFrame(html = suicide_attacks.iloc[i]['popup_text'], width = 400, height = 175)
        popup = folium.Popup(iframe, max_width = 400, parse_html = True)
        folium.CircleMarker([suicide_attacks.iloc[i]['latitude'], suicide_attacks.iloc[i]['longitude']], 
                      popup=popup, color = "red", radius = 1).add_to(fg2)
    my_map.add_child(fg2)  
    fg3 = folium.FeatureGroup(name = 'IDF Attacks', show = False)
    for i in range(0,len(idf_attacks)):
        iframe = folium.IFrame(html = idf_attacks.iloc[i]['popup_text'], width = 400, height = 175)
        popup = folium.Popup(iframe, max_width = 400, parse_html = True)
        folium.CircleMarker([idf_attacks.iloc[i]['latitude'], idf_attacks.iloc[i]['longitude']], 
                      popup=popup, color = "yellow", radius = 1).add_to(fg3)
    my_map.add_child(fg3)     
    fg4 = folium.FeatureGroup(name = 'IED Attacks', show = False)
    for i in range(0,len(ied_attacks)):
        iframe = folium.IFrame(html = ied_attacks.iloc[i]['popup_text'], width = 400, height = 175)
        popup = folium.Popup(iframe, max_width = 400, parse_html = True)
        folium.CircleMarker([ied_attacks.iloc[i]['latitude'], ied_attacks.iloc[i]['longitude']], 
                      popup=popup, color = "orange", radius = 1).add_to(fg4)
    my_map.add_child(fg4)
    folium.LayerControl().add_to(my_map)

    return my_map



