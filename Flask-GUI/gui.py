'''
This is a test GUI for the ABI workflow
'''
from flask import Flask , render_template , request
from abi import abi

app = Flask(__name__)


@app.route('/',methods=['GET','POST'])
def dropdown():
    provinces=['Badakhshan','Badghis','Baghlan','Balkh','Bamyan','Daykundi','Farah','Faryab','Ghazni','Ghor','Hilmand','Hirat','Jawzjan','Kabul','Kandahar','Kapisa','Khost','Kunar','Kunduz','Laghman','Logar','Nangarhar','Nimroz','Nuristan','Paktya','Paktika','Panjshir','Parwan','Samangan','Sari Pul','Takhar','Uruzgan','Wardak','Zabul']
    if request.method == 'POST':
        province = request.form.get("provinces", None)
        province = province.upper()
        date = request.form.get('date', None)
        map = abi(province , date)
        map.save('templates/map.html')

        return render_template('map_render.html',province=province,date=date)

    return render_template('index.html' , provinces=provinces )

@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500

if __name__ == '__main__':
    app.run()
