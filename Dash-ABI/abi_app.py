import dash
import dash_core_components as dcc
from dash.dependencies import Output , Input , State
import dash_html_components as html
import dash_table
import pandas as pd
import numpy as np
from datetime import datetime
import geopandas as gpd
import pygeohash as gh
import folium
from shapely import geometry
import math
import random
from abi import abi

app = dash.Dash(__name__)


app.layout = html.Div([
    html.Div(html.Img(src='/assets/logo800.jpg'), className='banner'),
    html.Div(html.H1(children="ABI webpage")),
    html.Div(dcc.Checklist(
                            id='province_input',
                            options=[{'label': 'Badakhshan', 'value': 'BADAKHSHAN'},{'label': 'Badghis', 'value': 'BADGHIS'}, {'label': 'Baghlan', 'value': 'BAGHLAN'},
                                     {'label': 'Balkh', 'value': 'BALKH'},{'label': 'Bamyan', 'value': 'BAMYAN'},{'label': 'Daykundi', 'value': 'DAYKUNDI'},
                                     {'label': 'Farah', 'value': 'FARAH'},{'label': 'Faryab', 'value': 'FARYAB'},{'label': 'Ghazni', 'value': 'GHAZNI'},
                                     {'label': 'Ghor', 'value': 'GHOR'},{'label': 'Helmand', 'value': 'HILMAND'},{'label': 'Hirat', 'value': 'HIRAT'},
                                     {'label': 'Jawzjan', 'value': 'JAWZJAN'},{'label': 'Kabul', 'value': 'KABUL'},{'label': 'Kandahar', 'value': 'KANDAHAR'},
                                     {'label': 'Kapisa', 'value': 'KAPISA'},{'label': 'Khost', 'value': 'KHOST'},{'label': 'Kunar', 'value': 'KUNAR'},
                                     {'label': 'Kunduz', 'value': 'KUNDUZ'},{'label': 'Laghman', 'value': 'LAGHMAN'},{'label': 'Logar', 'value': 'LOGAR'},
                                     {'label': 'Nangarhar', 'value': 'NANGARHAR'},{'label': 'Nimroz', 'value': 'NIMROZ'},{'label': 'Nuristan', 'value': 'NURISTAN'},
                                     {'label': 'Paktya', 'value': 'PAKTYA'},{'label': 'Paktika', 'value': 'PAKTIKA'},{'label': 'Panjshir', 'value': 'PANJSHIR'},
                                     {'label': 'Parwan', 'value': 'PARWAN'},{'label': 'Samangan', 'value': 'SAMANGAN'},{'label': 'Sari Pul', 'value': 'SARI PUL'},
                                     {'label': 'Takhar', 'value': 'TAKHAR'},{'label': 'Uruzgan', 'value': 'URUZGAN'},{'label': 'Wardak', 'value': 'WARDAK'},
                                     {'label': 'Zabul', 'value': 'ZABUL'}],
                            value=['BADAKHSHAN']
                            )),
    html.Div(dcc.DatePickerSingle(id='date_input',
                                placeholder= 'Select Date')),
    html.Div(html.Button('Submit', id='submit_val', n_clicks=0)),
    dcc.Loading(id = "loading-icon", children=
        [html.Div(html.Iframe(id='map',srcDoc = open('map_test.html','r').read(), width='90%', height='600'))],type='default')

])

@app.callback(
                Output('map','srcDoc'),
                [Input('province_input','value'),
                Input('date_input' , 'date'),
                Input('submit_val','n_clicks')]
            )
def gen_map(p_choice,d_choice, n_clicks):
    if n_clicks >0:
        try:
            abi(p_choice , d_choice)
            return open('my_map.html','r').read()
        except ValueError:
            return dash.no_update
    else:
        df = pd.DataFrame()
        return dash.no_update

if __name__ =="__main__":
    app.run_server(debug=True, use_reloader=False)
